"use strict";

var express = require("express");

var server = express();

var morgan = require("morgan");

var routerLivre = require("./routeurs/livres.routeur");

var routerGlobal = require("./routeurs/global.routeur");

var routerAuteur = require("./routeurs/auteurs.routeur");

var mongoose = require("mongoose");

var bodyParser = require("body-parser");

var session = require("express-session");

server.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 60000
  }
}));
mongoose.connect("mongodb://localhost/biblio2", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
server.use(express["static"]("public"));
server.use(morgan("dev"));
server.use(bodyParser.urlencoded({
  extended: false
}));
server.set('trust proxy', 1);
server.use(function (requete, reponse, suite) {
  reponse.locals.message = requete.session.message;
  delete requete.session.message;
  suite();
});
server.use("/livres/", routerLivre);
server.use("/auteurs/", routerAuteur);
server.use("/", routerGlobal);
server.listen(3000);