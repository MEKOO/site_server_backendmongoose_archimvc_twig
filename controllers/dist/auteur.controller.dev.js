"use strict";

var mongoose = require("mongoose");

var auteurSchema = require("../models/auteurs.modele");

var livreSchema = require("../models/livres.modele");

var fs = require("fs");

exports.auteur_affichage = function (requete, reponse) {
  auteurSchema.findById(requete.params.id).populate("livres").exec().then(function (auteur) {
    console.log(auteur);
    reponse.render("auteurs/auteur.html.twig", {
      auteur: auteur,
      isModification: false
    });
  })["catch"](function (error) {
    console.log(error);
  });
};

exports.auteurs_affichage = function (requete, reponse) {
  auteurSchema.find().populate("livres").exec().then(function (auteurs) {
    reponse.render("auteurs/liste.html.twig", {
      auteurs: auteurs
    });
  })["catch"]();
};

exports.auteurs_ajout = function (requete, reponse) {
  var auteur = new auteurSchema({
    _id: new mongoose.Types.ObjectId(),
    nom: requete.body.nom,
    prenom: requete.body.prenom,
    age: requete.body.age,
    sexe: requete.body.sexe ? true : false
  });
  auteur.save().then(function (resultat) {
    reponse.redirect("/auteurs");
  })["catch"](function (error) {
    console.log(error);
  });
};

exports.auteur_suppression = function (requete, reponse) {
  auteurSchema.find().where("nom").equals("anonyme").exec().then(function (auteur) {
    livreSchema.updateMany({
      "auteur": requete.params.id
    }, {
      "$set": {
        "auteur": auteur[0]._id
      }
    }, {
      "multi": true
    }).exec().then(auteurSchema.remove({
      _id: requete.params.id
    }).where("nom").ne("anonyme").exec().then(reponse.redirect("/auteurs"))["catch"]());
  });
};

exports.auteur_modification = function (requete, reponse) {
  auteurSchema.findById(requete.params.id).populate("livres").exec().then(function (auteur) {
    console.log(auteur);
    reponse.render("auteurs/auteur.html.twig", {
      auteur: auteur,
      isModification: true
    });
  })["catch"](function (error) {
    console.log(error);
  });
};

exports.auteur_modification_validation = function (requete, reponse) {
  var auteurUpdate = {
    nom: requete.body.nom,
    prenom: requete.body.prenom,
    age: requete.body.age,
    sexe: requete.body.sexe ? true : false
  };
  auteurSchema.update({
    _id: requete.body.identifiant
  }, auteurUpdate).exec().then(function (resultat) {
    reponse.redirect("/auteurs");
  })["catch"]();
};